import React, { useEffect, useLayoutEffect } from 'react';
import Square from './Square';
import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from './store';
import { nextStep, reset, toLoadGame, toSaveGame } from './board/actions'
import WinningModal from './WinningModal';
import { Redirect, useParams } from 'react-router-dom';
import './Board.css'
import { push } from 'connected-react-router';


function Board() {
    const squares = useSelector((state: IRootState) => state.board.squares);
    const oIsNext = useSelector((state: IRootState) => state.board.oIsNext);
    const id = useSelector((state: IRootState) => (state.board.previousBoards.length + 1));
    const { routeId } = useParams<{ routeId: string }>();

    const dispatch = useDispatch();

    useLayoutEffect(() => {
        if (!routeId) {
            return;
        } else if (routeId && 0 < parseInt(routeId) && parseInt(routeId) <= id) {
            dispatch(toLoadGame(parseInt(routeId)));
            return;
        } else if (isNaN(parseInt(routeId)) || parseInt(routeId) < 0 || parseInt(routeId) > id) {
            dispatch(push('/'))
        }
    }, [routeId, id]);

    // if (isNaN(parseInt(routeId)) || parseInt(routeId) < 0 || parseInt(routeId) > id) {
    //     return <Redirect to='/' />
    // }

    const calculateWinner = () => {
        const lines = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6],
        ];
        for (const line of lines) {
            const [a, b, c] = line;
            if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
                return {
                    winner: squares[a],
                    winningPattern: [a, b, c]
                };
            }
        }
        return null;
    }
    const handleClick = (i: number) => {
        const newSquares = [...squares];
        if (!newSquares[i] && !calculateWinner()) {
            const player = oIsNext ? 'O' : 'X';
            dispatch(nextStep(player, i));
        }
    }
    const renderSquare = (i: number) => {
        const isWinningPattern: boolean = (calculateWinner()?.winner) ? (calculateWinner()?.winningPattern as Array<number>).includes(i) : false
        return <Square inWinningPattern={isWinningPattern} value={squares[i]} squareOnClick={() => handleClick(i)} />;
    }
    const winner = calculateWinner()?.winner;
    const status: string = winner ? `Winner: ${winner}` : `Next player: ${oIsNext ? 'O' : 'X'}`;

    const genGrid = (i: number) => {
        i = 0;
        const grid: Array<Array<React.ReactElement>> = [];
        for (let j = 0; j < 3; j++) {
            const row = [];
            const col: Array<React.ReactElement> = [];
            let div = (<div className="board-row">{col}</div>);
            for (let k = 0; k < 3; k++) {
                col.push(renderSquare(i));
                i++
            }
            row.push(div);
            grid.push(row);

        }
        return grid
    }

    const tryAgain = () => {
        dispatch(reset())
    }

    const saveGame = () => {
        dispatch(toSaveGame(id, squares, oIsNext))
    };

    return (
        <div>
            <div className="status">{status}</div>
            {genGrid(9)}
            <button onClick={saveGame}>SAVE GAME</button>
            {winner ? <WinningModal tryAgain={tryAgain} winner={winner} /> : ''}
        </div>
    )
}

export default Board;