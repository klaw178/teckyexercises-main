import React, { useState } from 'react';
import './Square.css'

interface ISquareProps {
    value: string | null
    squareOnClick: () => void
    inWinningPattern: boolean
}

function Square(props: ISquareProps) {
    return (
        <button className={`square ${props.inWinningPattern ? 'winning-square' : ''}`} onClick={props.squareOnClick}>
            {props.value}
        </button>
    )
}

export default Square;