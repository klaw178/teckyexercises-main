import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'react-hook-form';
import { Modal, ModalHeader, ModalBody, ModalFooter, Button, Form, Label, Input } from 'reactstrap';
import { IRootState } from './store';
import { addWinner } from './score/actions';

interface IWinningModalProps {
    tryAgain: () => void
    winner: string
};

interface IWinningModalForm {
    name: string
    email: string
    gender: string
};



function WinningModal({ tryAgain, winner }: IWinningModalProps) {
    const squares = useSelector((state: IRootState) => state.board.squares);
    const { register, handleSubmit } = useForm<IWinningModalForm>({
        defaultValues: {
            name: '',
            email: '',
            gender: ''
        }
    });

    const dispatch = useDispatch();

    const onSubmit = (data: IWinningModalForm) => {
        console.log('form submit',data.name, data)
        dispatch(addWinner(data.name, squares, winner));
        tryAgain();
    };

    return (
        <Modal isOpen={true}>
            <ModalHeader>Congratulations!</ModalHeader>
            <ModalBody>
                Congratulations! {winner} win the Game!!
                Please input your name and email address here!!
                <Form onSubmit={handleSubmit(onSubmit)}>
                    <Label>
                        Name:
                        <input type='text' {...register('name')} />
                    </Label>
                    <Label>
                        Email
                        <input type='email' {...register('email')} />
                    </Label>
                    <Label>
                        Gender Group
                        <select {...register('gender')}>
                            <option value="">Please Select</option>
                            <option value="M">Male</option>
                            <option value="F">Female</option>
                        </select>
                    </Label>
                    <Input type='submit' value="Submit" />
                </Form>
            </ModalBody>
            <ModalFooter>
                <Button color="primary" onClick={tryAgain}>Try Again!</Button>
            </ModalFooter>
        </Modal>
    )
}

export default WinningModal