import {IPreviousBoard} from './reducers'

export interface IBoardState {
    squares: Array<string | null>;
    oIsNext: boolean;
    previousBoards: Array<IPreviousBoard | null>;
};