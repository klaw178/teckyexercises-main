import { IBoardState } from './state';
import { IBoardActions } from './actions';

interface IPreviousBoardState {
    squares: Array<string | null>;
    oIsNext: boolean
}

export interface IPreviousBoard {
    id: number
    value: IPreviousBoardState
}


const initialState = {
    squares: [],
    oIsNext: true,
    previousBoards: [],
};

const boardReducers = (state: IBoardState = initialState, action: IBoardActions) => {
    switch (action.type) {
        case "NEXT_STEP": {
            const { player, index } = action;
            const newSquares = [...state.squares]
            newSquares[index] = player;
            return {
                squares: newSquares,
                oIsNext: !state.oIsNext,
                previousBoards: state.previousBoards,

            }
        };
        case "RESET": {
            return {
                squares: [],
                oIsNext: true,
                previousBoards: state.previousBoards,

            };
        };
        case "TO_SAVE_GAME": {
            const squares = state.squares;
            const oIsNext = state.oIsNext;
            const id = action.id;
            const previousBoard = {
                id: id,
                value: {
                    squares: squares,
                    oIsNext: oIsNext,
                }
            };
            alert(`You game is saved! It is named Board ${id}.`)
            return {
                squares: squares,
                oIsNext: oIsNext,
                previousBoards: [...state.previousBoards, previousBoard],
            };
        };
        case "TO_LOAD_GAME": {
            const prevSquares = (state.previousBoards[action.routeId - 1] as IPreviousBoard).value.squares;
            const prevOIsNext = (state.previousBoards[action.routeId - 1] as IPreviousBoard).value.oIsNext;
            return {
                squares: prevSquares,
                oIsNext: prevOIsNext,
                previousBoards: state.previousBoards
            };
        };
        default:
            return state
    };
};

export default boardReducers