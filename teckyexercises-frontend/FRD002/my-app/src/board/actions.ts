export function nextStep(player: string, index: number) {
    return {
        type: 'NEXT_STEP' as const,
        player,
        index
    };
};

export function reset() {
    return {
        type: 'RESET' as const
    };
};

export function toSaveGame(id: number, squares: Array<string | null>, oIsNext: boolean) {
    return {
        type: 'TO_SAVE_GAME' as const,
        id,
        squares,
        oIsNext
    }
}

export function toLoadGame(routeId:number) {
    return {
        type: 'TO_LOAD_GAME' as const,
        routeId,
    }
}

export type IBoardActions = ReturnType<typeof nextStep | typeof reset | typeof toSaveGame | typeof toLoadGame>