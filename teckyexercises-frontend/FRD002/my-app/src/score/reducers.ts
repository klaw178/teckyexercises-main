import { IScoreState } from './state';
import { IScoreActions } from './actions';

const initialState = {
    scores: []
};

const scoreReducers = (state: IScoreState = initialState, action: IScoreActions): IScoreState => {
    switch (action.type) {
        case "ADD_WINNER":
            return {
                scores: [...state.scores,
                {
                    name: action.name,
                    squares: action.squares,
                    winner: action.winner
                }]
            }
        default:
            return state
    }
}

export default scoreReducers