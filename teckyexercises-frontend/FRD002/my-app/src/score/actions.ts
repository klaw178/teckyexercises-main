export function addWinner(name: string, squares: Array<string | null>, winner: string) {
    return {
        type: "ADD_WINNER" as const,
        name,
        squares,
        winner
    };
};

export type IScoreActions = ReturnType<typeof addWinner>;