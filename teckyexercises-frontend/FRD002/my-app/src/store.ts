import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import { IBoardState } from './board/state';
import { IBoardActions } from './board/actions';
import boardReducers from './board/reducers';
import { IScoreState } from './score/state';
import { IScoreActions } from './score/actions';
import scoreReducers from './score/reducers';
import logger from 'redux-logger';
import {
    RouterState,
    connectRouter,
    routerMiddleware,
    CallHistoryMethodAction
} from 'connected-react-router';
import { createBrowserHistory } from 'history';

export const history = createBrowserHistory();

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
    }
}

export interface IRootState {
    board: IBoardState;
    score: IScoreState;
    router: RouterState;
};

type IRootAction = IBoardActions | IScoreActions | CallHistoryMethodAction;

const rootReducer = combineReducers<IRootState>({
    board: boardReducers,
    score: scoreReducers,
    router: connectRouter(history),
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore<IRootState, IRootAction, {}, {}>(rootReducer, composeEnhancers(applyMiddleware(logger), applyMiddleware(routerMiddleware(history))));