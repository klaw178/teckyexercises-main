import React from 'react';
import Board from './Board';
import { Provider } from 'react-redux';
import store from './store';
import { ConnectedRouter } from 'connected-react-router';
import { history } from './store';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { NavLink, Route, Switch } from 'react-router-dom';
import Home from './Home';
import ScoreWinners from './Score';


function App() {
  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <div className="App">
          <nav className='nav-bar'>
            <NavLink to='/' className='link'>Home</NavLink>
            <NavLink to='/board' className='link'>Open Board</NavLink>
            <NavLink to='/scores' className='link'>Score Board</NavLink>
            <NavLink to='/about' className='link'>About Us</NavLink>
          </nav>
          <div>
            <Switch>
              <Route exact path='/'><Home /></Route>
              <Route exact path='/board/:routeId'>
                <div className='game'>
                  <div className='game-info'>
                    <Board />
                  </div>
                </div></Route>
              <Route exact path='/board'><div className='game'>
                <div className='game-info'>
                  <Board />
                </div>
              </div>
              </Route>
              <Route exact path='/scores'><ScoreWinners /></Route>
              <Route exact path='/about'>FUCK REACT REDUX</Route>
              <Route>404 Not Found</Route>
            </Switch>
          </div>

        </div>
      </ConnectedRouter>
    </Provider>
  );
}

export default App;
