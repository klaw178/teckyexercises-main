import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { Button, ListGroup, ListGroupItem } from 'reactstrap';
import { IRootState } from './store';
import { push } from 'connected-react-router';


const divStyle = {
    display: "flex",
    flexDirection: 'column' as 'column',
    alignItems: "center"
}

function Home() {
    const previousBoards = useSelector((state: IRootState) => state.board.previousBoards);

    const dispatch = useDispatch();

    const newBoard = () => {
        dispatch(push('/board'))
    };

    

    return (
        <div style={divStyle}>
            <h1 className="text-primary"> My TIC-TAC-TOE Game</h1>
            <p className="text-muted">Welcome to my tic-tac-toe game with history!
            </p>
            <Button onClick={newBoard}>Start New Game!</Button>
            {previousBoards.length !== 0 &&
                (<>
                    <h3>--- OR ---</h3>
                    <div>
                        Continue from one of the following game!
                <ListGroup>
                            {
                                previousBoards &&
                                previousBoards.map((previousBoard, i) => (
                                    <Link to={`/board/${previousBoard?.id}`}>
                                        <ListGroupItem key={i}>
                                            Board {i + 1}<br />
                                        </ListGroupItem>
                                    </Link>
                                ))
                            }
                        </ListGroup>
                    </div></>)}
        </div>
    )
}

export default Home;