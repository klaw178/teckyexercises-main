import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { IScore } from './score/state';
import { IRootState } from './store';

const spanStyle = {
    display: "flex",
    flexDirection: 'column' as 'column',
    alignItems: "center",
    marginTop: "20px",
    color: "black"
}

function ScoreWinners() {
    const winners = useSelector((state: IRootState) => state.score.scores)
    console.log(winners)
    // useEffect(() => {

    // }, [winners])

    return (
        <div>
            <span style={spanStyle}>Previous Winners ! {winners.length > 0 ? winners.map((winner: IScore) => (<p>{winner.name}</p>)) : 'None'}</span>
        </div>

    )
}

export default ScoreWinners