import React, { useEffect, useState} from 'react';

function Timer() {

    const [time, setTime] = useState<number>(0);
    const [capturedTime, setCapturedTime] = useState<number | null>(null);
    const [isFirstStart, setIsFirstStart] = useState<boolean>(true);
    const [isPause, setIsPause] = useState<boolean>(true);
    const formatTime = (time: number) => {
        return new Date(time * 1000).toISOString().substr(11, 8);
    };

    useEffect(() => {
        if (isPause) {
            return console.log('Stopped');
        };
        const id = setInterval(() => {
            setTime(prevTime => prevTime + 1)
        }, 1000);
        return () => {
            console.log('cleared!');
            clearInterval(id);
        };
    }, [isPause]);
    

    function handleStart() {
        setIsPause(prev => !prev);
    };
    function handleReset() {
        setTime(0);
        setCapturedTime(null);
        setIsFirstStart(true);
        setIsPause(true);
    };
    function handleCapture() {
        setCapturedTime(time)
    };
    function handleFirstStart() {
        setIsFirstStart(false);
    }
   function wrap() {
       handleStart();
       handleFirstStart();
   }
    return (
        <div>
            <span>{formatTime(time)}</span>
            <button onClick={wrap}>{isFirstStart? 'Start': isPause? 'Resume': 'Pause'}</button>
            <button onClick={isPause? handleReset: handleCapture}>{isPause? 'Reset': 'Lap'}</button>
            <span>{capturedTime? formatTime(capturedTime): ''}</span>
        </div>
    );
}

export default Timer