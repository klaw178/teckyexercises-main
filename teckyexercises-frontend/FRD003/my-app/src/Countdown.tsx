import React, { useEffect, useRef, useState } from 'react';

function Timer() {

    const [time, setTime] = useState<number>(0);
    const [inputValue, setInputValue] = useState<number>(0)
    const [isFirstStart, setIsFirstStart] = useState<boolean>(true);
    const [isPause, setIsPause] = useState<boolean>(true);
    const formatTime = (time: number) => {
        return new Date(time * 1000).toISOString().substr(11, 8);
    };

    useEffect(() => {
        if (isPause) {
            return console.log('Stopped');
        };
        const id = setInterval(() => {
            setTime(prevTime => prevTime - 1)
        }, 1000);
        return () => {
            console.log('cleared!');
            clearInterval(id);
        };
    }, [isPause]);


    function handleStart() {
        setIsPause(prev => !prev);
    };
    function handleReset() {
        setTime(0);
        setInputValue(0);
        setIsFirstStart(true);
        setIsPause(true);
    };
    function handleFirstStart() {
        setIsFirstStart(false);
    };
    function wrap() {
        handleStart();
        handleFirstStart();
    };

    function editTime(e: KeyboardEvent) {
        const inputValue = (e.target as HTMLSpanElement).textContent
        if (inputValue) {
            const seconds = inputValue.split(':').reduce((acc,time) => (60 * +acc) + +time, 0);
            console.log('check seconds', seconds)
            setTime(seconds);
            console.log(time, seconds)
        }
    }

    useEffect(() => {
        if (timeRef.current) {
            (timeRef.current).addEventListener('keydown', editTime);
        }
        return () => {
            (timeRef.current)?.removeEventListener('keydown', editTime);
        }
    }, []);

    useEffect(() => {
        console.log('updated', time)
    }, [time])

    const timeRef = useRef<HTMLSpanElement>(null);

    return (
        <div>
            <input value={isNaN(inputValue) ? '' : inputValue} onChange={(event) => {
                setInputValue(parseInt(event.target.value));
                setTime(parseInt(event.target.value));
            }} />
            <span contentEditable ref={timeRef} >{formatTime(isNaN(time) ? 0 : time)}</span>
            <button onClick={wrap}>{isFirstStart ? 'Start' : isPause ? 'Resume' : 'Pause'}</button>
            <button onClick={handleReset}>Reset</button>
        </div>
    );
}

export default Timer