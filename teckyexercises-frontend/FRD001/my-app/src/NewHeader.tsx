import React from 'react';

interface INewHeader {
    text: string
}

function NewHeader({ text }: INewHeader) {
    return (
        <header>
            <h1>
                {text}
            </h1>
        </header>
    )
}

export default NewHeader