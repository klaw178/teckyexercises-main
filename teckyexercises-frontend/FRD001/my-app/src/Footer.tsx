import React from 'react';

interface IImg {
    link: string
}

function Img({ link }: IImg) {
    return (
        <footer>
            <img src={link}></img>
        </footer>
    )
}

export default Img