import React from 'react';
import HighlightedText from './HighlightedText';

interface ISection {
    sectionProp: string,
    firstPlainP: string,
    secondPlainP: string,
    firstMarkup: string,
    secondMarkup: string
}

// sectionProp: ISection, firstProp: ISection, secondProp: ISection

function Section({ sectionProp, firstPlainP, secondPlainP, firstMarkup, secondMarkup }: ISection) {
    return (
        <section>
            {sectionProp}
            <ol>
                <li>{firstPlainP} <HighlightedText markupText={firstMarkup}/></li>
                <li>{secondPlainP} <HighlightedText markupText={secondMarkup}/></li>
            </ol>
        </section>
    )
}

export default Section