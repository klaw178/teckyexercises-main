import React from 'react';
import logo from './logo.svg';
import './App.css';
import NewHeader from './NewHeader';
import Section from './Section';
import Img from './Footer';

function App() {
  const text = 'Simple Website';
  const sectionText = 'This is a simple website made without React. Try to convert this into React enabled.';
  const ul1 = 'First you need to use';
  const ul2 = 'Second you need to run';
  const mkup1 = 'create-react-app';
  const mkup2 = 'npm start';
  const link = '/logo.png';

  return (
    <div className="App">
      <div className="exercise">
        <NewHeader text={text} />
        <Section sectionProp={sectionText} firstPlainP={ul1} secondPlainP={ul2} firstMarkup={mkup1} secondMarkup={mkup2} />
        <Img link={link} />
      </div>
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
