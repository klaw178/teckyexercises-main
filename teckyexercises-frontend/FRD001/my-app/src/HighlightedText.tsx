import React from 'react';
import styles from './HighlightedText.module.css';

interface IHighlightedText {
    markupText: string
}

function HighlightedText({markupText}: IHighlightedText) {

    return (
        <span className={styles.highlighted}>
            {markupText}
        </span>
    )
}

export default HighlightedText
