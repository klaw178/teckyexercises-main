// console.log('hihi')
import fs from 'fs';
import os from 'os';

import { asyncListAllJsRecursive, readLineInterfacePromise } from '/Users/kent/Desktop/teckyexercises-ts/WSP-C14-REV/functions';

const readCommand = async function () {
    while (true) {
        const answer = await readLineInterfacePromise("Please choose read the report(1) or run the benchmark(2)... exit via ctrl + c:   ");
        const option = parseInt(answer, 10);
        console.log(`Option ${answer} chosen.`);
        if (option == 1) {
            await readTheReport('./result.txt');
        } else if (option == 2) {
            await runTheBenchmark(10);
        } else {
            console.log('Please input 1 or 2 only.')
        }
    }
}

readCommand();

// type Report = Trial[]

interface Trial {
    startDate: string,
    endDate: string,
    timeNeeded: number,
    extraMemUsed: number,
    name: string,
}

class Report implements Trial {
    startDate: string;
    endDate: string;
    timeNeeded: number;
    extraMemUsed: number;
    name: string;
    constructor(startDate: string, endDate: string, timeNeeded: number, extraMemUsed: number, name: number) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.timeNeeded = timeNeeded;
        this.extraMemUsed = extraMemUsed;
        this.name = `${name} times`;
    }
}

function fsWriteFilePromise(file: string, content: string, options: { flag: string }) {
    return new Promise<void>(function (resolve, reject) {
        fs.writeFile(file, content, options, function (err: Error) {
            if (err) {
                reject(err);
                return;
            }
            resolve();
        })
    })
}

function fsReadFilePromise(file:string) {
    return new Promise<Buffer>(function (resolve, reject) {
        fs.readFile(file, function(err:Error, data:Buffer) {
            if(err){
                reject(err);
                return
            }
            resolve(data)
        })
    })
}

async function runTheBenchmark(runTimes: number) {
    let startDate = new Date();
    let endDate = new Date()
    let startTime = Date.now();
    let startMem = os.freemem();
    for (let i = 0; i < runTimes; i++) {
        await asyncListAllJsRecursive('/Users/kent/Desktop/teckyexercises-ts/WSP-C14-REV')
    }
    let endMem = os.freemem();
    let endTime = Date.now();
    let timeNeeded = endTime - startTime;
    let extraMemUsed = startMem - endMem;
    const report = new Report(startDate.toString(), endDate.toString(), timeNeeded, extraMemUsed, runTimes);
    await fsWriteFilePromise('./result.txt', JSON.stringify(report) + '\n', { flag: 'a+' })
}

async function readTheReport(file:string) {
    await fsReadFilePromise(file).then(function(data) {
        console.log(data.toString('utf-8'))
    })
}

