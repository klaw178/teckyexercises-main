import fs from 'fs';
import path from 'path';
import readline from 'readline';

export function readLineInterfacePromise(question: string) {
    return new Promise<string>(function (resolve, reject) {
        const readLineInterface = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        })
        readLineInterface.question(question, (answer: string) => {
            resolve(answer)
            readLineInterface.close();
        });
    })
}

export function fsReadDirPromise(dirPath: string): Promise<string[]> {
    return new Promise(function (resolve, reject) {
        fs.readdir(dirPath, function (err, files) {
            if (err) {
                reject(err);
                return;
            }
            resolve(files)
        })
    })
}

export function fsStatPromise(filePath: string): Promise<fs.Stats> {
    return new Promise(function (resolve, reject) {
        fs.stat(filePath, function (err, file) {
            if (err) {
                reject(err);
                return;
            }
            resolve(file)
        })
    })
}

export async function asyncListAllJsRecursive(folderPath: string) {
    let files = await fsReadDirPromise(folderPath);
    for (let file of files) {
        let address = path.join(folderPath, file)
        let result = await fsStatPromise(address);
        if (result.isFile() && path.extname(file) == '.js') {
            console.log(address)
        } else if (result.isDirectory()) {
            await asyncListAllJsRecursive(address)
        }
    }
}