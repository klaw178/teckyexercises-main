import fs from 'fs';

// when you order, it is not always ready
console.log('step a')
const returnValue = fs.readFile('quotes.txt', function(err, data){  // <- callback
    console.log('step b')
    // either error or data, no both
    if (err) {
        console.log('step c');
        console.log(err);
        return
    }
    console.log('step d');
    console.log(data.toString());
})
console.log('step e')
console.log("return value is "+ returnValue)
console.log('step f')

import os from 'os'

console.log(os.cpus())


