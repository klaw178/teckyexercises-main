// console.log('hihi')

import fs from 'fs';
import path from 'path';

fs.readdir(__dirname, function(err, files) {
    if (err) {
        console.log('Error');
    } else {
        for (let file of files) {
            if (path.extname(file) == '.js') {
                console.log(file)
            }
        }
    }
})

fs.stat(__dirname, function(err, stats) {
    if (err) {
        console.log('Error');
    } else {
        console.log(stats.isDirectory())
    }
})





function listAllJsRecursive(pathString: string) {
    fs.readdir(pathString, function(err, files) {
        if (err) {
            console.log(err);
            return;
        } else {
            for (let file of files) {
                let inside = path.join(pathString, file)
                fs.stat(inside, function(err, stats) {
                    if (err) {
                        console.log(err);
                        return;
                    } else {
                        if (!stats.isDirectory() && path.extname(file) == '.ts') {
                            console.log(inside)
                        } else if (stats.isDirectory()) {
                            listAllJsRecursive(inside);
                        }
                    }
                })
            }
        }
    })
}

listAllJsRecursive('/Users/kent/Desktop/teckyexercises-ts/WSP-C14-003')