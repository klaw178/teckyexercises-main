// class 係keyword
// Student 係convention上大楷

class Student {
    // Fields
    name: string;
    age: number;
    learningLevel: number

    // Methods

    // constructor <-- 行new ge時侯 第一個行ge method
    // initialize d fields
    // 係memory assign 一段for 呢舊instance

    constructor(name: string, age: number) {
        this.name = name;
        this.age = age;
        this.learningLevel = 0;
    }
    // learn & slack method 有一個hourSpent type係number ge parameter
    learn(hourSpent: number) {
        this.learningLevel += hourSpent * 0.3;
        this.slack(hourSpent / 4);
    }
    slack(hourSpent: number) {
        this.learningLevel -= hourSpent * 0.1
    }
}

const bob = new Student('Bob', 25)

const sarah = new Student('Sarah', 20)

bob.learn(10);
// SVO <- subject verb object
// just like 'bob learns 10 hours
bob.slack(5);

// 學寫coding ge student 有哂所有student ge field
// but NOT vice versa

class CodingStudent extends Student {

    constructor(name: string, age: number) {
        // if you are a subclass, u mush call superclass constructor
        // this must be called after super
        super(name, age)
        this.learningLevel = 10
    }

    //呢知learn method override jor Student ge learn method
    learn(hourSpent: number) {
        super.learn(10);
        this.learningLevel += hourSpent * 0.6
    }
    readReddit(hourSpent: number) {
        this.learningLevel += hourSpent * 0.05
    }
}

const annie = new CodingStudent('Annie', 19)
annie.learn(10);
annie.slack(5);

// bob.readReddit() will cause error cause bob is only a Student, not a CodingStudent

class TeckyStudent extends CodingStudent {
    slack(hourSpent: number) {
        this.learningLevel -= hourSpent * 0.2
    }
    // it doesnt override/implement learn 呢個 method
    // cannot remove inherited methods, 
    // you can ask to throw errors
}

const charlie = new TeckyStudent('Charlie', 18)

charlie.slack(5)






// interface just stating the method and nothing else, keep it as simple as possible
interface FlyingAnimal {
    // method type
    // 1. method name 
    // 2. parameters
    // 3. return value
    flyFor(hours: number): boolean
}

class Eagle implements FlyingAnimal {
    flyFor(hours: number): boolean {
        console.log(`Eagle can fly ${hours} hours with hot air stream`);
        return true;
    }
}

class Bat implements FlyingAnimal {
    flyFor(hours: number): boolean {
        console.log(`Bat can fly ${hours} hours with navigation with supersonic`);
        return true;
    }
}

class Dragonfly implements FlyingAnimal {
    flyFor(hours: number): boolean {
        console.log(`Dragonfly can fly ${hours} hours with insect wings`);
        return true;
    }
}

const eagle = new Eagle();
eagle.flyFor(3)

const bat = new Bat();
bat.flyFor(4)

const dragonfly = new Dragonfly();
dragonfly.flyFor(5)

function flyingContest(flyingAnimals: FlyingAnimal[]) {
    for (let flyingAnimal of flyingAnimals) {
        flyingAnimal.flyFor(10)
    }
}

flyingContest([eagle, bat, dragonfly])