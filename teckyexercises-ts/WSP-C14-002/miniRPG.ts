// // Declaration of Class and its methods
// class Player {
//     private name: string;
//     private strength: number;
//     private exp: number;

//     constructor(name: string, strength: number) {
//         this.name = name;
//         this.strength = strength;
//         this.exp = 0
//     }

//     attack(monster: Monster) {
//         if (monster.getHp() > 0) {
//             if (Math.random() < 0.2 && (monster.getHp() >= 40)) {
//                 monster.injure(this.strength * 2)
//                 console.log(this.name + ` attacks the monster (HP: ${monster.getHp()}) [CRITICAL] and gets exp ${this.exp}`)
//             } else {
//                 monster.injure(this.strength)
//                 console.log(this.name + ` attacks the monster (HP: ${monster.getHp()}) and gets exp ${this.exp}`)
//             }

//             this.gainExperience(10);

//         } else throw new Error("The monster has benn eliminated");
//     }

//     gainExperience(exp: number) {
//         this.exp += exp
//     }
// }


// class Monster {
//     private hp: number;

//     constructor(hp: number) {
//         this.hp = hp
//     }

//     injure(strength: number) {
//         this.hp -= strength
//     }

//     getHp() {
//         return this.hp
//     }
//     // Think of how to write injure
// }


// // Invocations of the class and its methods
// const peter = new Player('Peter', 20);
// const monster = new Monster(100);
// peter.attack(monster);
// peter.attack(monster);
// peter.attack(monster);



// // English counterpart: Player attacks monster