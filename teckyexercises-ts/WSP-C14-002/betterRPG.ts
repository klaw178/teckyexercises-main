interface Attack {
    damage: number;
}

class BowAndArrow implements Attack {
    damage: number
    constructor(damage: number) {
        this.damage = damage
    }
}

class ThrowingSpear implements Attack {
    damage: number
    constructor(damage: number) {
        this.damage = damage
    }
}

class MagicSpells implements Attack {
    damage: number
    constructor(damage: number) {
        this.damage = damage
    }
}

class Swords implements Attack {
    damage: number
    constructor(damage: number) {
        this.damage = damage
    }
}

interface Player {
    attack(monster: Monster): void;
    switchAttack(): void;
    gainExperience(exp: number): void;
}

class GenericPlayer implements Player {
    private primary: Attack;
    private secondary: Attack;
    private usePrimaryAttack: boolean;
    private exp: number;
    constructor(primary: Attack, secondary:Attack) {
        this.primary = primary;
        this.secondary = secondary;
        this.usePrimaryAttack = true;
        this.exp = 0
    }

    attack(monster: Monster): void {
        if (this.usePrimaryAttack) {
            monster.injure(this.primary.damage)
        } else {
            monster.injure(this.secondary.damage)
        }
        this.gainExperience(10);
    }

    switchAttack() {
        if (this.usePrimaryAttack) {
            this.usePrimaryAttack = false
        } else {
            this.usePrimaryAttack = true
        }
    }

    gainExperience(exp:number) {
        this.exp += exp
    }   
}

class Monster {
    private hp: number;

    constructor(hp: number) {
        this.hp = hp
    }

    injure(strength: number) {
        this.hp -= strength
    }

    getHp() {
        return this.hp
    }
}

function createPlayer(role:string) {
    if (role === 'Amazon') {
        return new GenericPlayer(new BowAndArrow(30), new ThrowingSpear(40))
    } else return null 
}

console.log(createPlayer('Amazon'))

// function flyToAnywhere(this: any) {
//     console.log(`${this.winner} can fly to ${this.destination}`)
// }
// let winnerInfo = {winner:'Sam', destination:'Shenzhen'}
// flyToAnywhere.bind(winnerInfo)()


