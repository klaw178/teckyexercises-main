import express from 'express'
import { Request, Response } from 'express';
import path from 'path';
import expressSession from 'express-session';
import bodyParser from 'body-parser';
// import jsonfile from 'jsonfile';
import multer from 'multer';
import http from 'http';
import { Server as SocketIO } from 'socket.io'
import { Client } from 'pg';
import dotenv from 'dotenv';
dotenv.config();

export const client = new Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
});

const app = express();
const server = new http.Server(app);
const io = new SocketIO(server);

io.on('connect', function (socket) {
    socket.on('submitMemo', function (data) {
        console.log(data);
        io.emit('newMemo', 'refresh')
    })
})

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve('./public/uploads'));
    },
    filename: function (req, file, cb) {
        cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
    }
});

const upload = multer({ storage });

app.use(expressSession({
    secret: 'Welcome',
    resave: true,
    saveUninitialized: true
}));

app.use(bodyParser.urlencoded({ extended: true }));

app.use(bodyParser.json());

// interface Memo {
//     content: string;
// }

// interface User {
//     emailAccount: string;
//     password: string;
// }

// app.get('/memos', async function (req, res) {
//     const memos = await jsonfile.readFile(path.resolve('./memos.json'));
//     res.json(memos)
// })

// app.post('/memo', upload.single('image'), async function (req, res) {

//     const memos: Memo[] = await jsonfile.readFile(path.resolve('./memos.json'));
//     if (req.file) {
//         req.body.image = req.file.filename
//     }
//     if (req.body.content != '' || req.file) {
//         memos.push(req.body);
//         res.json({ success: true });
//     } else {
//         res.json({ status: "you submit shit" });
//     }
//     await jsonfile.writeFile(path.resolve('./memos.json'), memos, { spaces: 4 })

// })

// app.post('/login', async function (req, res) {

//     const { emailAccount, password } = req.body;

//     const users: User[] = await jsonfile.readFile(path.resolve('./users.json'));

//     for (let user of users) {
//         if (user['emailAccount'] === emailAccount && user['password'] == password) {
//             req.session['user'] = true;
//             res.json({ message: "success" })
//             return
//         } else {
//             continue
//         }
//     }
//     res.status(400).json({ message: "Invalid email/password" });

// })

// app.delete('/memo/:id', async function (req, res) {
//     const memoIndex = parseInt(req.params.id);
//     console.log(memoIndex);
//     const memos: Memo[] = await jsonfile.readFile(path.resolve('./memos.json'));
//     if (memos.length > 0) {
//         memos.splice(memoIndex, 1);
//     } else {
//         res.json({ message: "No more memos!" })
//         return;
//     }
//     await jsonfile.writeFile(path.resolve('./memos.json'), memos, { spaces: 4 });
//     res.json({ success: "deleted" });
// })

// app.put('/memo/:id', async function (req, res) {
//     const memoIndex = parseInt(req.params.id);
//     const memos: Memo[] = await jsonfile.readFile(path.resolve('./memos.json'));
//     if (req.body.content != '') {
//         memos[memoIndex].content = req.body.content;
//         await jsonfile.writeFile(path.resolve('./memos.json'), memos, { spaces: 4 });
//         res.json({ message: 'success' })
//     } else {
//         res.status(400).json({ message: 'no text!' })
//     }
// })

app.get('/memos', async function (req, res) {

    const result = await client.query(/*sql*/`SELECT * FROM memos ORDER BY updated_at DESC`);
    res.json(result.rows);

}
)

app.post('/memo', upload.single('image'), async function (req, res) {

    if (req.body.content != '') {
        if (req.file) {
            req.body.image = req.file.filename;
            await client.query(/*sql*/`INSERT INTO memos (content,image,created_at,updated_at) VALUES ($1,$2,NOW(),NOW())`, [req.body.content, req.body.image]);
            res.status(200).json({ success: true });
            // io.emit('new-memo',req.body)
        } else {
            await client.query(/*sql*/`INSERT INTO memos (content,created_at,updated_at) VALUES ($1,NOW(),NOW())`, [req.body.content]);
            res.status(200).json({ success: true });
        }
    } else {
        res.status(400).json({ message: "No text!" });
    }

})

app.post('/login', async function (req, res) {

    const { emailAccount, password } = req.body;
    const result = await client.query(/*sql*/`SELECT * FROM users`);
    const users = result.rows;
    for (let user of users) {
        if (user.username === emailAccount && user.password === password) {
            req.session['user'] = true;
            res.json({ success: true });
            return
        } else {
            continue
        }
    }
    res.status(401).json({ message: 'Invalid email/passowrd' })

})

app.delete('/memo/:id', async function (req, res) {
    const memoId = parseInt(req.params.id);
    await client.query('DELETE FROM memos where id = $1', [memoId]);
    res.json({ success: true });
})

app.put('/memo/:id', async function (req, res) {

    const memoId = parseInt(req.params.id);
    if (req.body.content != '') {
        await client.query(/*sql*/`UPDATE memos set content = $1 , updated_at=NOW() where id = $2`, [req.body.content, memoId]);
        res.json({ success: true });
    } else {
        res.status(400).json({ message: 'No text!' })
    }

})

const isLoggedIn = function (req: express.Request, res: express.Response, next: express.NextFunction) {
    if (req.session && req.session['user']) {
        next();
    } else {
        res.redirect('/');
    }
}

app.use(express.static('public'));

app.use(isLoggedIn, express.static('protected'));

app.use('/', function (req: Request, res: Response) {
    res.sendFile(path.resolve('./public/404.html'));
})

client.connect().then(function () {
    server.listen(8080, function () {
        console.log('Listening at http://localhost:8080')
    })
})

