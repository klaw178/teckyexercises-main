import {Client} from 'pg';
import dotenv from 'dotenv';
import xlsx from 'xlsx';
dotenv.config();

export const client = new Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
});

interface User{
    username:string
    password:string
}

interface Memo{
    content:string
    image: string
}

async function main() {
    await client.connect();
    const workbook = xlsx.readFile('./WSP009-exercise.xlsx');
    const users:User[] = xlsx.utils.sheet_to_json(workbook.Sheets['user']);
    //console.log(users)
    for (let user of users) {
        await client.query(/*sql*/`insert into users (username,password,created_at,updated_at) values ($1,$2,NOW(),NOW())`,[user.username,user.password]);
    }
    const memos:Memo[] = xlsx.utils.sheet_to_json(workbook.Sheets['memo']);
    for (let memo of memos) {
        await client.query(/*sql*/`insert into memos (content,image,created_at,updated_at) values ($1,$2,NOW(),NOW())`,[memo.content,memo.image]);
    }


    await client.end();
}

main();