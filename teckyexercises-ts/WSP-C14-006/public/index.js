const socket = io.connect();
loadMemos()

document.querySelector('#memo-form').addEventListener('submit', async function (event) {
    event.preventDefault();

    const form = event.target;
    const memo = new FormData()
    memo.append('content', form.content.value);
    if (form.image.files.length > 0) {
        memo.append('image', form.image.files[0]);
    }
    const res = await fetch('/memo', {
        method: 'POST',
        body: memo
    })
    const result = await res.json()
    alert(JSON.stringify(result))
    socket.emit('submitMemo', 'submitted')
    form.reset();
    await loadMemos();
})

async function loadMemos() {
    const res = await fetch('/memos');
    const memos = await res.json();
    const memosContainer = document.querySelector('.wall');
    memosContainer.innerHTML = '';
    for (let index in memos) {
        const memo = memos[index];
        memosContainer.innerHTML += `
        <div class="memo">
            <div class="memoInput" contenteditable>
                ${memo.content}
                ${(memo.image) ?
                `<img src='uploads/${memo.image}'>`
                : ""
            }
            </div>
            <div class="removeBtnContainer" memo-index="${memo.id}">  
                <i class="fas fa-trash-alt removeBtn"></i>
            </div>
            <div class="editBtnContainer" memo-index="${memo.id}">
                <i class="fas fa-edit editBtn"></i>
            </div>
        </div>
        `
    }

    // use index when using json method
    // use memo.id when using sql

    const editBtns = document.querySelectorAll('.editBtnContainer');
    for (let editBtn of editBtns) {
        editBtn.addEventListener('click', async function (event) {

            const memoIndex = event.currentTarget.getAttribute("memo-index");
            const memoContent = event.currentTarget.closest('.memo').innerText
            const res = await fetch(`/memo/${memoIndex}`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    content: memoContent
                })
            })
            const result = await res.json();
            if (res.status == 200) {
                loadMemos();
            } else {
                alert("Update failed: " + result.message);
                loadMemos();
            }
        })
    }

    const deleteBtns = document.querySelectorAll('.removeBtnContainer');
    for (let deleteBtn of deleteBtns) {
        deleteBtn.addEventListener('click', async function (event) {
            const memoIndex = event.currentTarget.getAttribute("memo-index");
            const memoContent = event.currentTarget.closest('.memo').innerText
            const res = await fetch(`/memo/${memoIndex}`, {
                method: "DELETE"
            })
            const result = await res.json();
            if (res.status == 200) {
                loadMemos();
            } else {
                alert("Delete failed");
                loadMemos();
            }
        })
    }
}

document.querySelector('#login-form').addEventListener('submit', async function (event) {
    event.preventDefault();

    const form = event.target;
    const user = {};
    user['emailAccount'] = form.emailAccount.value;
    user['password'] = form.password.value;
    const res = await fetch('/login', {
        method: 'POST',
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(user)
    })
    const result = await res.json()
    if (res.status === 200) {
        window.location = '/admin.html';
    } else {
        alert(result.message)
    }
    form.reset();
})

socket.on('newMemo', async function(data) {
    console.log(data)
    await loadMemos();
})