// console.log('hihi')

import fs from 'fs';
import path from 'path';
import readline from 'readline';
import papa from 'papaparse';

// Promisify 
export function fsReadDirPromise(dirPath: string): Promise<string[]> {
    return new Promise(function (resolve, reject) {
        fs.readdir(dirPath, function (err, files) {
            if (err) {
                reject(err);
                return;
            }
            resolve(files)
        })
    })
}

export function fsStatPromise(filePath: string): Promise<fs.Stats> {
    return new Promise(function (resolve, reject) {
        fs.stat(filePath, function (err, file) {
            if (err) {
                reject(err);
                return;
            }
            resolve(file)
        })
    })
}

export async function asyncListAllJsRecursive(folderPath: string) {
    let files = await fsReadDirPromise(folderPath);
    for (let file of files) {
        let address = path.join(folderPath, file)
        let result = await fsStatPromise(address);
        if (result.isFile() && path.extname(file) == '.js') {
            console.log(address)
        } else if (result.isDirectory()) {
            asyncListAllJsRecursive(address)
        }
    }
}

// asyncListAllJsRecursive('/Users/kent/Desktop/teckyexercises-ts/WSP-C14-004')


export async function asyncListAllJsRecursive2(folderPath: string) {
    fsReadDirPromise(folderPath)
        .then(function (files) {
            let filePath: string[] = [];
            for (let file of files) {
                filePath.push(path.join(folderPath, file))
            }
            return filePath
        })
        .then(async function (filePath) {
            for (let address of filePath) {
                let result = await fsStatPromise(address);
                if (result.isFile() && path.extname(address) == '.js') {
                    console.log(address)
                } else if (result.isDirectory()) {
                    asyncListAllJsRecursive2(address)
                }
            }
        })
        .catch(function (err) {
            console.log(err)
        })
}

// asyncListAllJsRecursive2('/Users/kent/Desktop/teckyexercises-ts/WSP-C14-004')

function setTimeoutPromise(ms: number) {
    return new Promise(function (resolve, reject) {
        setTimeout(function (err) {
            if (err) {
                reject(err);
            }
            console.log('hi');
            resolve(' ')
        }, ms)
    })
}

setTimeoutPromise(5000).then(function (resolved) { // value in the resolve(), which is ' ' is passed to variable: resolved
    console.log(resolved + ' hi') // result is 
})                                // 'hi
//                                      hi'

export function sleep(ms: number) {  // sleep can be used to temporary stop a loop 
    return new Promise<void>(function (resolve, reject) {
        setTimeout(function () {
            resolve()
        }, ms)
    })
}

export function readLineInterfacePromise(question: string) {
    return new Promise<string>(function (resolve, reject) {
        const readLineInterface = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        })
        readLineInterface.question(question, (answer: string) => {
            console.log(`Your name is ${answer}`);
            resolve(answer)
            readLineInterface.close();
        });
    })
}


export function papaparsePromise(source: string) {
    return new Promise(function (resolve, reject) {
        const file = fs.createReadStream(source);
        papa.parse(file, {
            worker: true,
            complete: function (results) {
                resolve(results)
            },
            error: function (err) {
                reject(err)
            }
        })
    })
}

papaparsePromise('./anycsv.csv').then(function (results) {
    console.log(results)
})
