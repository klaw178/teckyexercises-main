const data = [
  {
    "name": "Hong Kong",
    "topLevelDomain": [
      ".hk"
    ],
    "alpha2Code": "HK",
    "alpha3Code": "HKG",
    "callingCodes": [
      "852"
    ],
    "capital": "City of Victoria",
    "altSpellings": [
      "HK",
      "香港"
    ],
    "region": "Asia",
    "subregion": "Eastern Asia",
    "population": 7324300,
    "latlng": [
      22.25,
      114.16666666
    ],
    "demonym": "Chinese",
    "area": 1104.0,
    "gini": 53.3,
    "timezones": [
      "UTC+08:00"
    ],
    "borders": [
      "CHN"
    ],
    "nativeName": "香港",
    "numericCode": "344",
    "currencies": [
      {
        "code": "HKD",
        "name": "Hong Kong dollar",
        "symbol": "$"
      }
    ],
    "languages": [
      {
        "iso639_1": "en",
        "iso639_2": "eng",
        "name": "English",
        "nativeName": "English"
      },
      {
        "iso639_1": "zh",
        "iso639_2": "zho",
        "name": "Chinese",
        "nativeName": "中文 (Zhōngwén)"
      }
    ],
    "translations": {
      "de": "Hong Kong",
      "es": "Hong Kong",
      "fr": "Hong Kong",
      "ja": "香港",
      "it": "Hong Kong",
      "br": "Hong Kong",
      "pt": "Hong Kong",
      "nl": "Hongkong",
      "hr": "Hong Kong",
      "fa": "هنگ‌کنگ"
    },
    "flag": "https://restcountries.eu/data/hkg.svg",
    "regionalBlocs": [],
    "cioc": "HKG"
  }
]

const hk = data[0];

//ok
// for(let key in hk){
//   if((key === 'languages') || (key === 'currencies')){
//     for(let nestedKey in hk[key]){
//       for(let deepKey in hk[key][nestedKey]){
//         console.log(key.toUpperCase().substring(0, 1) + key.substring(1) + "_" + deepKey + ": " + hk[key][nestedKey][deepKey])
//       }
//     }
//   } else if (key === 'translations'){
//     for(let nestedKey in hk[key]){
//       console.log(key.toUpperCase().substring(0, 1) + key.substring(1) + "_" + nestedKey + ": " + hk[key][nestedKey])
//     }
//   } else {
//     console.log(key.toUpperCase().substring(0, 1) + key.substring(1) + ": " + hk[key])
//   }
// }

// missing "regionalBlocs"
for (let key in hk) {
  if (hk[key] instanceof Array) {
    for (let nestedKey of hk[key]) {  // looping the array, if there's nth in the array, it loops n times...
      if (nestedKey instanceof Object) {
        for (let deepKey in nestedKey) {
          console.log(key + '_' + deepKey + ': ' + nestedKey[deepKey])
        }
      } else {
        console.log(key + ': ' + nestedKey)
      }
    }
  } else if (hk[key] instanceof Object) {
    for (let nestedKey in hk[key]) {
      console.log(key + '_' + nestedKey + ': ' + hk[key][nestedKey])
    }
  } else {
    console.log(key + ': ' + hk[key])
  }
}




