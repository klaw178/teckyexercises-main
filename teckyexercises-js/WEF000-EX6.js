
function checkMarkSix(array, bid){
    let check = []
    for(let number of bid){
        check.push(array.includes(number))
    }
    if(check[0] == true && check[1] == true ){
        return true
    }else{
        return false
    }
}

// console.log(checkMarkSix([2, 4, 10, 15, 14, 19], [2, 19]))
function quickPicks(array, bidTimes){
    const resultArray = []
    for(i = 0; i < bidTimes; i++){
        const result = {};
        const bidNum = [Math.round(Math.random() * 10), Math.round(Math.random() * 10)];
        result["bid"] = bidNum;
        result["win"] = checkMarkSix(array, bidNum);
        resultArray.push(result)
    }
    return resultArray
}

console.log(quickPicks([1, 3, 5, 7, 9, 11], 3))


