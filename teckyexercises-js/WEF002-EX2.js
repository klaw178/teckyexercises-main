const cards = [
    ['Spade','A'],
    ['Diamond','J'],
    ['Club','3'],
    ['Heart','6'],
    ['Spade','K'],
    ['Club','2'],
    ['Heart','Q'],
    ['Spade','6'],
    ['Heart','J'],
    ['Spade','10'],
    ['Club','4'],
    ['Diamond','Q'],
    ['Diamond','3'],
    ['Heart','4'],
    ['Club','7']
];

function compareCard(cardA, cardB){
    const ranks = ["2","3","4","5","6","7","8","9","10","J","Q","K","A"];
    const suits = ["Diamond","Club","Heart","Spade"];
    const [suitA,rankA] = cardA;
    const [suitB, rankB] = cardB;
    const ranksDiff = ranks.indexOf(rankA) - ranks.indexOf(rankB);
    if(ranksDiff !== 0){
      return ranksDiff;
    }else{
      return suits.indexOf(suitA) - suits.indexOf(suitB);
    }
}

//#1
const count1 = cards.filter(function(card){
    return card.includes('Spade')
})
console.log(count1.length)

const count2 = cards.reduce(function(count, card){
    if (card[0] == 'Spade') {
        count++
    } return count 
},0)

console.log(count2)

//#2
const newDeck2 = cards.filter(function(card){
    if(compareCard(['Club', '3'], card) <= 0){
        return card
    }
}
) 
console.log(newDeck2)

//#3
const count3 = cards.filter(function(card){
    if(compareCard(card, ['Diamond', 'J']) >= 0){
        return card
    }
}
).filter(function(card){
    if(card.includes('Diamond') || card.includes('Heart')){
        return card
    }
})
console.log(count3.length)

//#4
const newDeck4 = cards.map(function(card){
    if(card.includes('Club') == true){
        return ['Diamond', card[1]]
    }else{
        return card
    }
})
console.log(newDeck4)

//#5
const newDeck5 = cards.map(function(card){
    if(card.includes('A') == true){
        return [card[0],'2']
    }else{
        return card
    }
})
console.log(newDeck5)

