function year(year){
    if(((year % 4) == 0) && ((year % 100) !== 0)){
        console.log(year + " is a leap year.")
    }else if((year % 400) == 0){
        console.log(year + " is a leap year.")
    }else{
        console.log(year + " is not a leap year.")
    }
}
year(1997)
year(1996)
year(1900)
year(2000)