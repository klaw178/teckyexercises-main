function reverseString(string){
    
    let reversedArray = []
    for(i = string.split('').length - 1; i >= 0; i-- ){
        reversedArray.push(string.split('')[i])
    }
    
    return reversedArray.join('')
}

console.log(reverseString("cool"))


// let string = "cool"
// let reversedArray = []
// for(i = string.split('').length - 1; i >= 0; i--){
//     reversedArray.push(string.split('')[i])
// }
// console.log(reversedArray.join(''))