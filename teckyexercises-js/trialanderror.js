// function accumulator () {
//     let sum = 0;

//     return {
//         reset: function () {
//             sum = 0;
//             return sum;
//         },
//         add: function (num) {
//             sum += num;
//             return sum;
//         },
//         result: function () {
//             return sum;
//         }
//     }
// }

// const acc = accumulator()

// acc.add(2)
// acc.reset()
// acc.result()

// console.log(acc.result())

// closure ... function in function
// acc becomes the name of the returned function
// that's why you can't print the first accumulator function, only acc function can return the sum

const data = [
    {
        "name": "Hong Kong",
        "topLevelDomain": [
            ".hk"
        ],
        "alpha2Code": "HK",
        "alpha3Code": "HKG",
        "callingCodes": [
            "852"
        ],
        "capital": "City of Victoria",
        "altSpellings": [
            "HK",
            "香港"
        ],
        "region": "Asia",
        "subregion": "Eastern Asia",
        "population": 7324300,
        "latlng": [
            22.25,
            114.16666666
        ],
        "demonym": "Chinese",
        "area": 1104.0,
        "gini": 53.3,
        "timezones": [
            "UTC+08:00"
        ],
        "borders": [
            "CHN"
        ],
        "nativeName": "香港",
        "numericCode": "344",
        "currencies": [
            {
                "code": "HKD",
                "name": "Hong Kong dollar",
                "symbol": "$"
            }
        ],
        "languages": [
            {
                "iso639_1": "en",
                "iso639_2": "eng",
                "name": "English",
                "nativeName": "English"
            },
            {
                "iso639_1": "zh",
                "iso639_2": "zho",
                "name": "Chinese",
                "nativeName": "中文 (Zhōngwén)"
            }
        ],
        "translations": {
            "de": "Hong Kong",
            "es": "Hong Kong",
            "fr": "Hong Kong",
            "ja": "香港",
            "it": "Hong Kong",
            "br": "Hong Kong",
            "pt": "Hong Kong",
            "nl": "Hongkong",
            "hr": "Hong Kong",
            "fa": "هنگ‌کنگ"
        },
        "flag": "https://restcountries.eu/data/hkg.svg",
        "regionalBlocs": [],
        "cioc": "HKG"
    }
]

const hk = data[0]

let str = ''
for (let key in hk) {
    str += key + ': ';
    if (hk[key] instanceof Array) {
        for (let item of hk[key]) {
            if (item instanceof Object) {
                str += '\n';
                for (let nestedKey in item) {
                    str += key + '_' + nestedKey + ': ' + item[nestedKey] + '\n'
                }
            } else {
                str += item + ' '
            }
        }
    } else if (hk[key] instanceof Object) {
        str += "\n"
        for (let nestedKey in hk[key]) {
            str += key + '_' + nestedKey + ': ' + hk[key][nestedKey] + '\n'
        }
    } else {
        str += hk[key]
    }
    str += '\n'
}



console.log(str)