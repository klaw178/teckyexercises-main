// function rnaTranscription(dnaStr){
//     let rnaArray = [];
//     for(i = 0; i < dnaStr.split("").length; i++){
//         if(dnaStr.split("")[i] == "G"){
//             rnaArray.push("C")
//         }else if(dnaStr.split("")[i] == "C"){
//             rnaArray.push("G")
//         }else if(dnaStr.split("")[i] == "T"){
//             rnaArray.push("A")
//         }else if(dnaStr.split("")[i] == "A"){
//             rnaArray.push("U")
//         }
//     };
//     return rnaArray.join('')
// }

// console.log(rnaTranscription("GCTAGCT"))


const translation = {
    G: "C",
    C: "G",
    T: "A",
    A: "U"
}

function rnaTranscription2(dna){
    return dna.split('').map(function(key){
        return translation[key]
    }).join('')
}

console.log(rnaTranscription2("GCTAGCT"))
