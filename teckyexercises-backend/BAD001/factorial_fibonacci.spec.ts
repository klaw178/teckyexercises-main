import { factorial, fibonacci } from './factorial_fibonaaci';

describe('factorial', function () {
    it('should be 1', function () {
        expect(factorial(1)).toBe(1)
    });
    it('should be 362880', function () {
        expect(factorial(9)).toBe(362880)
    });
})

describe('fibonacci', function () {
    let fibonacci_seq = [];
    for (let i = 0; i <= 10; i++) {
        fibonacci_seq.push(fibonacci(i));
    }
    it('should be ', function () {
        expect(fibonacci_seq).toEqual([0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55])
    });
})