export function fizzBuzz (num:Number):string[] {
    let outPut: string[] = [];
    for (let i = 1; i <= num; i++) {
        if (i % 3 == 0 && i % 5 == 0) {
            outPut.push('Fizz Buzz');
        } else if (i % 3 == 0) {
            outPut.push('Fizz');
        } else if (i % 5 == 0) {
            outPut.push('Buzz');
        }
        else outPut.push(i.toString());
    };
    return outPut
}