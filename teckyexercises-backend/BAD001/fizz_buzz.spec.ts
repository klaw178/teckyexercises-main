import { fizzBuzz } from './fizz_buzz';

let players: Number;
let outPut: string[] = [];
const expectedOutPut: string[] = ['1', '2', 'Fizz', '4', 'Buzz', 'Fizz', '7', '8', 'Fizz', 'Buzz', '11', 'Fizz', '13', '14', 'Fizz Buzz']

describe('checking the sequence of fizz buzz', function () {
    it('should print 1', function() {
        expect(fizzBuzz(1)[0]).toEqual('1')
    });
    it('should print "Fizz"', function() {
        expect(fizzBuzz(3)[2]).toEqual('Fizz');
    });
    it('should print "Buzz"', function() {
        expect(fizzBuzz(5)[4]).toEqual('Buzz');
    });
    it('should print "Fizz Buzz"', function() {
        expect(fizzBuzz(15)[14]).toEqual('Fizz Buzz');
    });
    it('should print expectedOutPut', function() {
        expect(fizzBuzz(15)).toEqual(expectedOutPut);
    });
})