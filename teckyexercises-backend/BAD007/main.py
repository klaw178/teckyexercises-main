
#%%
import math
import sklearn
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report



class KNNClassifier:

    def __init__(self, X_train, y_train, n_neighbors = 3):
        self.n_neighbors = n_neighbors
        self._X_train = X_train
        self._y_train = y_train

    """calculate the euclidean distance here"""
    def euclidean_dist(self, point_1, point_2):
        # n = n-th dimension
        sq = 0
        for n in range(0, len(point_1)):
            sq += (point_1[n] - point_2[n])**2
        return math.sqrt(sq)

    """accept multiple inputs here and predict one by one with predict_single()"""
    def predict(self, X_test_array):
        pred_list = []
        for i in range(0, len(X_test_array)):
            v = self.predict_single(X_test_array[i])
            pred_list.append(v)
        return pred_list

    """predict single input here"""
    def predict_single(self, input_data_single):
        dist_list = []
        vote_list = []
        for i in range(0, len(self._X_train)):
            d = self.euclidean_dist(input_data_single, self._X_train[i])
            label = self._y_train[i]
            dist_list.append([d,label])
        
        dist_list.sort(key=lambda x: x[0])
        for j in range(0, 2):
            vote_list.append(dist_list[j][1])
        most_common = max(vote_list, key=vote_list.count)
        return most_common



iris = load_iris()
X = iris.data
y = iris.target

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# print(X_train.shape)    # (120, 4)
# print(X_test.shape)     # (30, 4)
# print(y_train.shape)    # (120,)
# print(y_test.shape)     # (30,)

iris_knn_classifier = KNNClassifier(X_train, y_train)
y_pred = iris_knn_classifier.predict(X_test)
print(classification_report(y_test, y_pred, target_names=[
      'Iris-Setosa', 'Iris-Versicolour', 'Iris-Virginica']))
# %%
