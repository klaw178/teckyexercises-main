#%%
import os
import tensorflow as tf
import matplotlib.pyplot as plt

print(f"tf version is: {tf.__version__}")

train_dataset_url = "https://storage.googleapis.com/download.tensorflow.org/data/iris_training.csv"
train_dataset_fp = tf.keras.utils.get_file(fname=os.path.basename(train_dataset_url),origin=train_dataset_url)
print("Local copy of the dataset file: {}".format(train_dataset_fp))

test_dataset_url = "https://storage.googleapis.com/download.tensorflow.org/data/iris_test.csv"
test_dataset_fp = tf.keras.utils.get_file(fname=os.path.basename(test_dataset_url),
                origin=test_dataset_url)
print("Local copy of the test dataset file: {}".format(test_dataset_fp))

#%% Preprocess Data
column_names = ['sepal_length', 'sepal_width', 'petal_length', 'petal_width', 'species']

feature_names = column_names[:-1]
label_name = column_names[-1]

print("Features: {}".format(feature_names))
print("Label: {}".format(label_name))

batch_size = 150
raw_train_dataset = tf.data.experimental.make_csv_dataset(train_dataset_fp,batch_size,column_names=column_names,label_name=label_name,num_epochs=1)

raw_features, raw_labels = next(iter(raw_train_dataset))
print(raw_features)
plt.scatter(raw_features['petal_length'],
    raw_features['sepal_length'],
    c=raw_labels,
    cmap='viridis')

# plt.xlabel("Petal length")
# plt.ylabel("Sepal length")
# plt.show()

#%% Transform dataset shape
def pack_features_vector(features, labels):
    features = tf.stack(list(features.values()), axis=1)
    return features, labels

train_dataset = raw_train_dataset.map(pack_features_vector)
features, labels = next(iter(train_dataset))

print(features)

raw_test_dataset = tf.data.experimental.make_csv_dataset(test_dataset_fp,batch_size,column_names=column_names,label_name='species',num_epochs=1,shuffle=False)
test_dataset = raw_test_dataset.map(pack_features_vector)
