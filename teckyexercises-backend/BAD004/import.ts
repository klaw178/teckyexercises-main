import Knex from 'knex';
import dotenv from "dotenv";
import xlsx from "xlsx";

dotenv.config();

const knexConfigs = require('./knexfile');
const configMode = process.env.NODE_ENV || "development";
const knexConfig = knexConfigs[configMode]
const knex = Knex(knexConfig)

interface User {
    username: string;
    password: string;
    level: string
}

interface Category {
    name: string
}

interface CustomFile {
    name: string;
    Content: string;
    is_file: number;
    category: string;
    owner: string;
}

async function main() {

    const workbook = xlsx.readFile("./BAD004-exercise_1.xlsx");

    
    const userBase: object = {};
    const categoryBase: object = {};

    await knex.transaction(async (txn) => {

        const users: User[] = xlsx.utils.sheet_to_json(workbook.Sheets["user"]);
        for (let user of users) {

            const userId: number = await txn.insert({
                username: user.username,
                password: user.password,
                level: user.level,
                created_at: new Date(),
                updated_at: new Date(),
            }).into('users').returning('id');
            console.log(userId);
            userBase[user.username] = userId[0];

        }

        const categories: Category[] = xlsx.utils.sheet_to_json(workbook.Sheets["category"]);
        for (let category of categories) {

            const categoryId: number = await txn.insert({
                name: category.name,
                created_at: new Date(),
                updated_at: new Date(),
            }).into('categories').returning('id');
            console.log(categoryId);
            categoryBase[category.name] = categoryId[0];

        }

        const files: CustomFile[] = xlsx.utils.sheet_to_json(workbook.Sheets["file"]);
        for (let file of files) {

            await txn.insert({
                name: file.name,
                Content: file.Content,
                is_file: file.is_file,
                category: categoryBase[file.category],
                owner: userBase[file.owner],
                created_at: new Date(),
                updated_at: new Date(),
            }).into('files');

        }

    })

    await knex.destroy()
}

main();