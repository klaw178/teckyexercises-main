import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    return knex.schema.createTable('files',table=>{
        table.increments();//id
        table.string('name');
        table.text('Content');
        table.integer('is_file');
        table.integer('category').unsigned().notNullable();
        table.foreign('category').references('categories.id');
        table.integer('owner').unsigned().notNullable();
        table.foreign('owner').references('users.id');
        table.timestamps(false,true);
    });
}


export async function down(knex: Knex): Promise<void> {
    return knex.schema.dropTable('files');
}

