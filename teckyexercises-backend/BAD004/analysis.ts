import Knex from 'knex';
import dotenv from "dotenv";

dotenv.config();

const knexConfigs = require('./knexfile');
const configMode = process.env.NODE_ENV || "development";
const knexConfig = knexConfigs[configMode]
const knex = Knex(knexConfig)


async function analysis() {

    console.log(await knex.select('users.username').count('files.id').from('files').innerJoin('users', 'files.owner', 'users.id').groupBy('users.username'))   

    console.log(await knex.select('categories.name').count('files.id').from('files').innerJoin('categories', 'files.category', 'categories.id').groupBy('categories.name'))   

    console.log(await knex.select('users.username','categories.name').count('files.id').from('files').innerJoin('users', 'files.owner', 'users.id').innerJoin('categories', 'files.category', 'categories.id').where('users.username','alex').andWhere('categories.name','Important').groupBy('users.username','categories.name'))   

    console.log(await knex.select('users.username').count('files.id').from('files').innerJoin('users', 'files.owner', 'users.id').groupBy('users.username').havingRaw('count(files.id) > ?', [800]))   

    await knex.destroy()
}

analysis();