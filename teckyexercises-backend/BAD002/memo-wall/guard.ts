import express, { Request, Response } from "express";

export const isLoggedIn = (
    req: Request,
    res: Response,
    next: express.NextFunction
) => {
    if (req.session["user"]) {
        //called Next here
        next();
    } else {
        res.redirect("/");
        // redirect to index page
    }
};

export const isLoggedInAPI = (
    req: Request,
    res: Response,
    next: express.NextFunction
) => {
    if (req.session["user"]) {
        //called Next here
        next();
    } else {
        res.status(401).json({ message: "Unauthorized" });
    }
};
