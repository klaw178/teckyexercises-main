import express from "express";
import expressSession from "express-session";
import path from "path";
import multer from "multer";
import http from "http";
import { Server as SocketIO } from "socket.io";
import Knex from 'knex';
import dotenv from "dotenv";
import { checkPassword, hashPassword } from "./hash";
import { isLoggedInAPI, isLoggedIn } from "./guard";
import grant from "grant";
import fetch from "node-fetch";
import crypto from "crypto";
import { User } from "./services/models";
import { MemoService } from './services/MemoService';
import { MemoController } from './controllers/MemoController';
import { LikeService } from "./services/LikeService";
import { LikeController } from "./controllers/LikeController";
const oauthSignature = require("oauth-signature");

dotenv.config();

const knexConfigs = require('./knexfile');
const configMode = process.env.NODE_ENV || "development";
const knexConfig = knexConfigs[configMode]
const knex = Knex(knexConfig)

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.resolve("./public/uploads"));
    },
    filename: function (req, file, cb) {
        cb(
            null,
            `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`
        );
    },
});

export const upload = multer({ storage });

// 提提你: Socket.io 不是HTTP protocol ，亦唔係express 嘅一部份
// express 係一個library
const app = express();
// http 係node.js native 嘅 module 去起server，而express 係基於http
const server = new http.Server(app);
export const io = new SocketIO(server);

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const sessionMiddleware = expressSession({
    secret: "Memo wall is fun",
    resave: true,
    saveUninitialized: true,
});

app.use(sessionMiddleware);

// Similar to a middleware for socket.io
io.use((socket, next) => {
    const request = socket.request as express.Request;
    sessionMiddleware(
        request,
        request.res as express.Response,
        next as express.NextFunction
    );
});

const grantExpress = grant.express({
    defaults: {
        origin: "http://localhost:8080",
        transport: "session",
        state: true,
    },
    google: {
        key: process.env.GOOGLE_CLIENT_ID || "",
        secret: process.env.GOOGLE_CLIENT_SECRET || "",
        scope: [
            "profile",
            "email",
            "https://www.googleapis.com/auth/drive.scripts",
        ],
        callback: "/login/google",
    },
    twitter: {
        key: process.env.TWITTER_CLIENT_ID || "",
        secret: process.env.TWITTER_CLIENT_SECRET,
        scope: ["profile", "email"],
        callback: "/login/twitter",
    },
});

app.use(grantExpress as express.RequestHandler);

// app.use 而無route，就係所有request都會做嘅middleware
app.use((req, res, next) => {
    if (req.session["counter"]) {
        // 有嘢就加一
        req.session["counter"] += 1;
    } else {
        //無嘢就set 做1
        req.session["counter"] = 1;
    }
    logger.debug(`req.session[counter] is ${req.session["counter"]}`);

    next();
});

app.use((req, res, next) => {
    const now = new Date();
    // const date = now.toISOString().replace('T',' ').replace(/\..*/g,'');
    const date = formatDate(now);
    logger.debug(`[${date}] Request ${req.path}`);
    next();
});

app.get("/current-user", isLoggedInAPI, async function (req, res) {
    const userFound = req.session["user"];
    if (userFound) {
        res.json({
            username: userFound.username,
        });
    } else {
        //唔suppose 去到呢個位
        res.status(401).json({ message: "Unauthorized" });
    }
});

export const memoService = new MemoService(knex);
export const memoController = new MemoController(memoService, io);
export const likeService = new LikeService(knex);
export const likeController = new LikeController(likeService)

// 呢個要寫得後過佢depends on 嘅variable
import { memoRoutes } from "./memoRoutes";
import { likeRoutes } from "./likeRoutes";
import { logger } from "./logger";




app.use("/", memoRoutes);
app.use("/", likeRoutes);

// Local login: Username + Password
app.post("/login", async (req, res, next) => {
    console.log(req.body); // 試真，係咪入到嚟先

    const users: User[] = (
        await knex.select('*').from('users').where('username', req.body.username))

    // 成功就是 User，失敗就是undefined
    const userFound = users[0];
    if (!userFound) {
        res.status(401).json({
            success: false,
            message: "Incorrect Username/Password",
        });
        // res.redirect('/login.html?error=Incorrect+Username+Password')
        return;
    }
    const match = await checkPassword(req.body.password, userFound.password);

    if (match) {
        req.session["user"] = userFound; // 之後你可以係req.session 度，access返個user
        // res.redirect('/index.html')
        res.json({ success: true });
    } else {
        // res.redirect('/login.html?error=Incorrect+Username+Password')
        res.status(401).json({
            success: false,
            message: "Incorrect Username/Password",
        });
    }
});

// Social Login: Username + user info from google
app.get("/login/google", async (req, res) => {
    const accessToken = req.session?.["grant"].response.access_token;
    const fetchRes = await fetch(
        "https://www.googleapis.com/oauth2/v2/userinfo",
        {
            headers: {
                Authorization: `Bearer ${accessToken}`,
            },
        }
    );
    const result = await fetchRes.json();

    const users = (await knex.select('*').from('users').where('users.username', result.email));

    let user = users[0];
    if (!user) {
        // lazy work
        user = {
            username: result.email,
            password: await hashPassword(crypto.randomBytes(30).toString()),
        };

        await knex.insert({
            username: user.username,
            password: user.password,
            created_at: new Date(),
            updated_at: new Date(),
        }).into('users');
    }
    req.session["user"] = user;
    res.redirect("/");
});

app.get("/login/twitter", async (req, res) => {
    // const accessToken = req.session?.['grant'].response.access_token;
    console.log(req.session?.["grant"]);
    const accessToken = req.session?.["grant"].response.access_token;
    const accessSecret = req.session?.["grant"].response.access_secret;

    const httpMethod = "GET";
    const url = "https://api.twitter.com/1.1/account/verify_credentials.json";
    const timestamp = new Date().getTime() / 1000; // in seconds
    const parameters = {
        oauth_consumer_key: process.env.TWITTER_CLIENT_ID,
        oauth_token: accessToken,
        oauth_nonce: "kllo9940pd9333jh",
        oauth_timestamp: timestamp,
        oauth_signature_method: "HMAC-SHA1",
        oauth_version: "1.0",
        include_email: true,
    };
    const consumerSecret = process.env.TWITTER_CLIENT_SECRET;
    const tokenSecret = accessSecret;
    const signature = oauthSignature.generate(
        httpMethod,
        url,
        parameters,
        consumerSecret,
        tokenSecret
    );
    console.log(signature);
    const fetchRes = await fetch(
        `https://api.twitter.com/1.1/account/verify_credentials.json?include_email=true&oauth_consumer_key=${process.env.TWITTER_CLIENT_ID}&oauth_nonce=kllo9940pd9333jh&oauth_signature_method=HMAC-SHA1&oauth_token=${accessToken}&oauth_timestamp=${timestamp}&oauth_version=1.0&oauth_signature=${signature}`
    );
    const result = await fetchRes.json();

    const users = (await knex.select('*').from('users').where('users.username', result.email))

    let user = users[0];
    if (!user) {
        // lazy work
        user = {
            username: result.email,
            password: await hashPassword(crypto.randomBytes(30).toString()),
        };

        await knex.insert({
            username: user.username,
            password: user.password,
            created_at: new Date(),
            updated_at: new Date(),
        }).into('users');
    }

    req.session["user"] = user;
    res.redirect("/");
});

app.get("/logout", (req, res) => {
    // 只要剷咗session 內的user ，guard 就會阻止你
    delete req.session["user"];
    res.redirect("/");
});

// public 要放在 protected 前面，否則isLoggedIn
// 你唔可以protect 埋login page
app.use(express.static("public"));
app.use(isLoggedIn, express.static("protected"));

app.use((req, res) => {
    res.sendFile(path.resolve("./public/404.html"));
});

//用咗socket.io ，就要改做server.listen
server.listen(8080, () => {
    console.log("Listening at http://localhost:8080");
});

io.on("connection", function (socket) {
    // 必然係 callback style
    if (!(socket.request as express.Request).session["user"]) {
        socket.disconnect();
    }

    // 呢段code 係每條socket connect 果陣行一次
    const user: User | undefined = (socket.request as express.Request).session[
        "user"
    ];
    if (user) {
        socket.join(`user-${user.username}`);
        // One common way is to join the socket to a room named by the `user.id` or other group information.
    }
});

function formatDate(now: Date) {
    return (
        now.getFullYear() +
        "-" +
        (now.getMonth() + 1 + "").padStart(2, "0") +
        "-" +
        (now.getDate() + "").padStart(2, "0") +
        " " +
        (now.getHours() + "").padStart(2, "0") +
        ":" +
        (now.getMinutes() + "").padStart(2, "0") +
        ":" +
        (now.getSeconds() + "").padStart(2, "0")
    );
}
