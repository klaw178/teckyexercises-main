import { loadMemos, getCurrentUser } from "./lib.js";
import "./login-form.js";
import "./memo-form.js";

// 連接咗嘅Socket.IO socket

export let socket;

export function initSocket() {
    socket = io.connect();
    socket.on("new-memo", async function (data) {
        await loadMemos();
    });
}

initSocket();

window.onload = async () => {
    await getCurrentUser();
    // loadMemo
    await loadMemos();
};
