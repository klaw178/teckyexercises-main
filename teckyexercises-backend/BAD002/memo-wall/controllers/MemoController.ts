import express from 'express';
import { MemoService } from '../services/MemoService';
import { Memo } from '../services/models';
import { Server as SocketIO } from "socket.io";

export class MemoController {
    constructor(private memoService: MemoService, private io: SocketIO) {

    }

    postMemos = async (req: express.Request, res: express.Response) => {
        const memo: Memo = req.body; // 不一定要直接對個req.body 落去
        let rows: { id: number }[];
        if (req.file) {
            rows = await this.memoService.postMemosWithFile(memo.content, req.file.filename);
            memo.image = req.file.filename;
        } else {
            rows = await this.memoService.postMemosWithoutFile(memo.content);
        }
        memo.id = rows[0].id;
        this.io.emit("new-memo", memo);
        res.json({ id: rows[0].id });
    }

    getMemos = async (req: express.Request, res: express.Response) => {
        const q = req.query.q;
        if (!q) {
            res.json(await this.memoService.listMemo());
        } else {
            const filteredMemos = await this.memoService.searchMemos(q + "");
            res.json(filteredMemos);
        }
    }

    deleteMemo = async (req: express.Request, res: express.Response) => {
        const id = parseInt(req.params.id); //<-- 必然係string
        if (isNaN(id)) {
            res.status(400).json({ message: "id is not an integer" });
            return;
        }
        await this.memoService.deleteMemo(id);
        res.json({ success: true });
    }

    updateMemo = async (req: express.Request, res: express.Response) => {
        const id = parseInt(req.params.id);
        if (isNaN(id)) {
            res.status(400).json({ message: "id is not an integer" });
            return;
        }
        await this.memoService.updateMemo(req.body.content, id);
        this.io.to(`user-${req.session["user"].username}`).emit("memo-updated", {
            message: "memo-updated!!",
        });
        res.json({ success: true });
    }
}