import Knex from 'knex';
import dotenv from "dotenv";
import xlsx from "xlsx";

dotenv.config();

const knexConfigs = require('./knexfile');
const configMode = process.env.NODE_ENV || "development";
const knexConfig = knexConfigs[configMode]
const knex = Knex(knexConfig)

interface User {
    username: string;
    password: string;
}

interface Memo {
    content: string;
    image: string;
}

async function main() {

    // 1. 用xlsx 讀到個內容先
    const workbook = xlsx.readFile("./WSP009-exercise.xlsx");

    const users: User[] = xlsx.utils.sheet_to_json(workbook.Sheets["user"]);

    // 2. Loop over 讀入嚟嘅內容
    for (let user of users) {
        // 3. 然後逐個insert
        await knex.insert({
            username: user.username,
            password: user.password,
            created_at: new Date(),
            updated_at: new Date(),
        }).into('users')
    }

    const memos: Memo[] = xlsx.utils.sheet_to_json(workbook.Sheets["memo"]);

    // 2. Loop over 讀入嚟嘅內容
    for (let memo of memos) {
        // 3. 然後逐個insert
        await knex.insert({
            content: memo.content,
            image: memo.image,
            created_at: new Date(),
            updated_at: new Date(),
        }).into('memos')
    }

    await knex.destroy()
}

main();
