let grids = [...document.querySelectorAll('.box')]
let data;

function init() {
    let x = 0;
    let y = 0;
    data = [[], [], []];
    turn = 0;
    for (let grid of grids) {
        grid.x = 0;
        grid.y = 0;
        grid[y][x] = 0;
        x++;
        if (x > 2) {
            y++;
            x = 0;
        }
        grid.addEventListener('click', clicked)
    }
}

function countScore(board) {
    let win = 0;
    for (let y = 0; y < board.length; y++) {
        let score = 0;
        for (let x = 0; x < board.length; x++) {
            score += board[y][x];
        }
        if (score == +3) win = 1;
        if (score == -3) win = -1;
    }
    for (let x = 0; x < board.length; x++) {
        let score = 0;
        for (let y = 0; y < board.length; y++) {
            score += board[y][x];
        }
        if (score == +3) win = 1;
        if (score == -3) win = -1;
    }
    let score1 = 0;
    let score2 = 0;
    for (let i = 0; i < board.length; i++) {
        score1 += board[i][i];
        score2 += board[board.length - i - 1][i];
        if (score1 == +3) win = 1;
        if (score1 == -3) win = -1;
        if (score2 == +3) win = 1;
        if (score2 == -3) win = -1;
    }
    return win
}

function checkDraw(board) {
    let count0 = 0;
    for (let y = 0; y < board.length; y++) {
        for (let x = 0; x < board.length; x++) {
            if (board[y][x] == 0) count0++
        }
    }
    return (count0 == 0)
}

init();