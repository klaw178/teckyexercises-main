// automated slideshow
// let slideshows = document.querySelectorAll('[data-component="slideshow"]')

// slideshows.forEach(initSlideShow)

// function initSlideShow(slideshow) {
//     let slides = document.querySelectorAll(`#${slideshow.id} [role="list"] .slide`)

//     let index = 0;
//     let time = 4000;

//     slides[index].classList.add('active')

//     setInterval (function () {

//         slides[index].classList.remove('active');
//         index++;
//         if (index === slides.length) {
//             index = 0
//         }
//         slides[index].classList.add('active');
//     }, time)
// }

// manual slideshow 

// let button1 = document.querySelector('.b1')
// let button2 = document.querySelector('.b2')
// let button3 = document.querySelector('.b3')
// let slide1 = document.querySelector('.slide1')
// let slide2 = document.querySelector('.slide2')
// let slide3 = document.querySelector('.slide3')

// button1.addEventListener('click', function () {
//     slide1.classList.add('active');
//     slide2.classList.remove('active');
//     slide3.classList.remove('active');

// })
// button2.addEventListener('click', function () {
//     slide1.classList.remove('active');
//     slide2.classList.add('active');
//     slide3.classList.remove('active');
// })
// button3.addEventListener('click', function () {
//     slide1.classList.remove('active');
//     slide2.classList.remove('active');
//     slide3.classList.add('active');
// })

// using for-loop to do manual slideshow 

// let slides = [...document.querySelectorAll('.slide')]
// // console.log(slides)

// let btns = [...document.querySelectorAll('.btn')]
// // console.log(btns)

// slides[0].classList.add('active');

// for (let btn of btns) {
//     btn.addEventListener('click', function () {
//         slides[btns.indexOf(btn)].classList.add('active');
//         // console.log(btns.indexOf(btn));
//         for (let slide of slides) {
//             // console.log(slide);
//             if (slides.indexOf(slide) !== btns.indexOf(btn)) {
//                 slide.classList.remove('active')
//             }
//         }
//     })
// }

//for-loop to do manual + auto navigation



let slides = [...document.querySelectorAll('.slide')]

let btns = [...document.querySelectorAll('.btn')]

slides[0].classList.add('active');
let index = 0;
let interval = setInterval(autoPlay, 5000);

for (let btn of btns) {
    btn.addEventListener('click', function () {
        slides[btns.indexOf(btn)].classList.add('active');
        index = btns.indexOf(btn);
        for (let slide of slides) {
            if (slides.indexOf(slide) !== btns.indexOf(btn)) {
                slide.classList.remove('active')
            }
        }
        clearInterval(interval);
        interval = setInterval(autoPlay, 5000);

    })
}

function autoPlay() {
    for (let slide of slides) {
        slide.classList.remove('active')
    }
    index++;
    if (index === slides.length) {
        index = 0
    };
    slides[index].classList.add('active')  
}
